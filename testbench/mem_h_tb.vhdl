LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY mem_h_tb IS
END mem_h_tb;

ARCHITECTURE behavior OF mem_h_tb IS
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT mem_h
    PORT 
    (
        clk: IN std_logic;
        address: IN std_logic_vector (3 DOWNTO 0);  -- address
        data_out: OUT std_logic_vector (7 DOWNTO 0)  -- data out
    );
    END COMPONENT;
    
	--Inputs
	SIGNAL clk : STD_LOGIC := '0';
	SIGNAL address : std_logic_vector(3 DOWNTO 0);
    --Outputs
    SIGNAL data_out : STD_LOGIC_VECTOR(7 downto 0);

BEGIN
    -- Instantiate the Unit Under Test (UUT)
    uut: mem_h PORT MAP (
        clk => clk,
		address => address,
        data_out => data_out
    );
	
	clk <= not clk after 10 ns;
	
    stim_proc: PROCESS
    BEGIN
        WAIT FOR 5 ns;
        address <= "0000";
		WAIT FOR 15 ns;
		address <= "0001";
		WAIT FOR 15 ns;
		address <= "0010";
		WAIT FOR 15 ns;
		address <= "0011";
		WAIT FOR 15 ns;
		address <= "0100";
		WAIT FOR 15 ns;
		address <= "0101";
		WAIT FOR 15 ns;
		address <= "0110";
		WAIT FOR 15 ns;
		address <= "0111";
		WAIT FOR 15 ns;
		address <= "1000";
		WAIT FOR 15 ns;
		address <= "1001";
		WAIT FOR 15 ns;
		address <= "1010";
		WAIT FOR 15 ns;
		address <= "1011";
		WAIT FOR 15 ns;
		address <= "1100";
		WAIT FOR 15 ns;
		address <= "1101";
		WAIT FOR 15 ns;
		address <= "1110";
		WAIT FOR 15 ns;
		address <= "1111";
    END PROCESS;
END behavior;
