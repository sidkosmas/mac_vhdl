LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

USE IEEE.MATH_REAL."ceil";
USE IEEE.MATH_REAL."log2";

ENTITY mac_top_tb IS
END mac_top_tb;

ARCHITECTURE behavior OF mac_top_tb IS
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT mac_top
    Generic (RES_SIZE : INTEGER);
	Port(
		clk : in std_logic;
		input : in std_logic_vector(7 downto 0);
		enable : in std_logic;
		reset : in std_logic;
		result : out std_logic_vector(RES_SIZE downto 0);
		mul_complete : out std_logic
	);
    END COMPONENT;
    
	--Inputs
	SIGNAL clk : STD_LOGIC := '0';
	SIGNAL input : std_logic_vector(7 downto 0);
	SIGNAL enable : std_logic;
	SIGNAL reset : STD_LOGIC := '0';
    --Outputs
    SIGNAL mul_complete : std_logic;
	SIGNAL result : std_logic_vector(15 + integer(ceil(log2(real(4)))) downto 0);

BEGIN
    -- Instantiate the Unit Under Test (UUT)
    uut: mac_top 
	Generic map (15 + integer(ceil(log2(real(4)))))
	PORT MAP (
        clk => clk,
		input => input,
		enable => enable,
		reset => reset,
		result => result,
        mul_complete => mul_complete
    );
	
	clk <= not clk after 2 ns;
	
    stim_proc: PROCESS
    BEGIN
		reset <= '0';
		
		wait for 20 ns;
		input <= "00000000";
		enable <= '1';
		wait for 10 ns;
		enable <= '0';
		
		wait for 20 ns;
		input <= "00000100";
		enable <= '1';
		wait for 10 ns;
		enable <= '0';
		
		wait for 20 ns;
		input <= "00100100";
		enable <= '1';
		wait for 10 ns;
		enable <= '0';
		
		wait for 20 ns;
		input <= "01101100";
		enable <= '1';
		wait for 10 ns;	
		enable <= '0';
		
		wait for 5000 ns;
    END PROCESS;
END behavior;
