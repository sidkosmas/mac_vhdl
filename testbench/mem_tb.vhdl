LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY mem_tb IS
END mem_tb;

ARCHITECTURE behavior OF mem_tb IS
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT mem
    GENERIC (N: INTEGER:= 2; BIT_SIZE: INTEGER:= 8);
	PORT 
    (
        clk: IN STD_LOGIC;
		we : IN STD_LOGIC;
		address : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
		din : IN STD_LOGIC_VECTOR(BIT_SIZE-1 DOWNTO 0);
		dout : OUT STD_LOGIC_VECTOR(BIT_SIZE-1 DOWNTO 0)
    );
    END COMPONENT;
    
	--Inputs
	SIGNAL clk : STD_LOGIC := '0';
	SIGNAL address : std_logic_vector(1 DOWNTO 0);
	SIGNAL din : std_logic_vector(7 DOWNTO 0);
	SIGNAL we : STD_LOGIC := '0';
    --Outputs
	SIGNAL dout : std_logic_vector(7 DOWNTO 0);

BEGIN
    -- Instantiate the Unit Under Test (UUT)
    uut: mem PORT MAP (
        clk => clk,
		we => we,
        address => address,
		din => din,
		dout => dout
    );
	
	clk <= not clk after 10 ns;
	
    stim_proc: PROCESS
    BEGIN
        wait for 30 ns;
		we <= '1';
		address <= "00";
		din <= "10101010";
		wait for 20 ns;
		we <= '0';
		wait for 20 ns;
		we <= '1';
		address <= "01";
		din <= "10101011";
		wait for 20 ns;
		we <= '0';
		wait for 20 ns;
		we <= '1';
		address <= "10";
		din <= "10101100";
		wait for 20 ns;
		we <= '0';
		wait for 20 ns;
		we <= '1';
		address <= "11";
		din <= "10101101";
		wait for 20 ns;
		we <= '0';
		wait for 20 ns;
		we <= '1';
		address <= "00";
		din <= "10101110";
		wait for 20 ns;
		we <= '0';
		wait for 60 ns;
		address <= "00";
		din <= "11111111";
		wait for 20 ns;
		we <= '0';
		din <= "11111111";
		wait for 20 ns;
		we <= '0';
		din <= "11111111";
		
		
		
		
    END PROCESS;
END behavior;
