vcom src/*.vhdl
vcom testbench/*.vhdl

vsim mac_top_tb

add wave *
add wave -position end  sim:/mac_top_tb/uut/ui_mem_a/mem
add wave -position end  sim:/mac_top_tb/uut/ui_mem_r/mem
add wave -position 7  sim:/mac_top_tb/uut/ui_mem_h/mem
add wave -position end  sim:/mac_top_tb/uut/A
add wave -position end  sim:/mac_top_tb/uut/B
add wave -position end  sim:/mac_top_tb/uut/mul_result
add wave -position end  sim:/mac_top_tb/uut/r_accumulator
add wave -position 1  sim:/mac_top_tb/uut/state
add wave -position end  sim:/mac_top_tb/uut/a_index_counter
add wave -position end  sim:/mac_top_tb/uut/h_index_counter
add wave -position end  sim:/mac_top_tb/uut/r_index_counter
add wave -position end  sim:/mac_top_tb/uut/r_write
add wave -position end  sim:/mac_top_tb/uut/a_write

run 350 ns