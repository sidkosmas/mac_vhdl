LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY mem_h IS
    PORT 
    (
        clk: IN std_logic;
        address: IN std_logic_vector (3 DOWNTO 0);  -- address
        data_out: OUT std_logic_vector (7 DOWNTO 0)  -- data out
    );
END mem_h;
ARCHITECTURE arc_ram OF mem_h IS
    CONSTANT RAM_DEPTH :INTEGER := 2**4;
    SIGNAL data :STD_LOGIC_VECTOR (7 DOWNTO 0);
    TYPE RAM IS ARRAY (INTEGER RANGE <>)OF STD_LOGIC_VECTOR (7 DOWNTO 0);
    SIGNAL mem : RAM (0 TO RAM_DEPTH-1);
BEGIN
    data_out <= data;
	mem(0) <= "00000100";
	mem(1) <= "00001000";
	mem(2) <= "00001100";
	mem(3) <= "00000011";
	
	mem(4) <= "00101010";
	mem(5) <= "00000010";
	mem(6) <= "11111101";
	mem(7) <= "00001011";
	
	mem(8) <= "00110110";
	mem(9) <= "11000000";
	mem(10) <= "00101110";
	mem(11) <= "00110000";
	
	mem(12) <= "01001110";
	mem(13) <= "01100010";
	mem(14) <= "01100001";
	mem(15) <= "00001000";
	
    PROCESS(clk) 
    BEGIN
        IF (RISING_EDGE(clk)) then
            data <= mem(conv_integer(address));
        END IF;
    END PROCESS;
END arc_ram;
