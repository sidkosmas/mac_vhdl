LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
use ieee.numeric_std.all;

ENTITY mul IS
    PORT 
    (
        A: IN std_logic_vector (7 DOWNTO 0);  -- data in
        B: IN std_logic_vector (7 DOWNTO 0);  -- data in
        data_out: OUT std_logic_vector (15 DOWNTO 0)  -- data out
    );
END mul;
ARCHITECTURE arc_mul OF mul IS
BEGIN
    data_out <= (A * B);
END arc_mul;
