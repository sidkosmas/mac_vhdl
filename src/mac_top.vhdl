LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

USE IEEE.MATH_REAL."ceil";
USE IEEE.MATH_REAL."log2";

--integer(ceil(log2(real(4)))) : 4 is for the rows of H

ENTITY mac_top IS
	Generic (RES_SIZE : INTEGER);
	Port(
		clk : in std_logic;
		input : in std_logic_vector(7 downto 0);
		enable : in std_logic;
		reset : in std_logic;
		result : out std_logic_vector(RES_SIZE downto 0);
		mul_complete : out std_logic
	);
END mac_top;

ARCHITECTURE arc_mac_top OF mac_top IS
	
	component mem
	GENERIC (N: INTEGER; BIT_SIZE: INTEGER);
    PORT 
    (
        clk: IN STD_LOGIC;
		we : IN STD_LOGIC;
		address : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
		din : IN STD_LOGIC_VECTOR(BIT_SIZE-1 DOWNTO 0);
		dout : OUT STD_LOGIC_VECTOR(BIT_SIZE-1 DOWNTO 0)
    );
	end component;
	
	component mem_h
	PORT 
    (
        clk: IN std_logic;
        address: IN std_logic_vector (3 DOWNTO 0);  -- address
        data_out: OUT std_logic_vector (7 DOWNTO 0)  -- data out
    );
	end component;
	
	component mul
	PORT 
    (
        A: IN std_logic_vector (7 DOWNTO 0);  -- data in
        B: IN std_logic_vector (7 DOWNTO 0);  -- data in
        data_out: OUT std_logic_vector (15 DOWNTO 0)  -- data out
    );
	end component;
	
	SIGNAL A : std_logic_vector(7 downto 0);	--A_MEM_OUT
	SIGNAL B : std_logic_vector(7 downto 0);	--H_MEM_OUT
	
	SIGNAL a_address : std_logic_vector(1 downto 0);
	SIGNAL h_address : std_logic_vector(3 downto 0);
	SIGNAL r_address : std_logic_vector(1 downto 0);
	
	SIGNAL mul_result : std_logic_vector(15 downto 0);
	
	SIGNAL r_write : std_logic;
	SIGNAL a_write : std_logic;
	
	SIGNAL r_accumulator : std_logic_vector(15 + integer(ceil(log2(real(4)))) downto 0);
	
	SIGNAL a_index_counter : integer := 0;	-- 0 to 15 for 1x4 4x4 matrices
	SIGNAL h_index_counter : integer := 0;	-- 0 to 15 for 1x4 4x4 matrices
	SIGNAL r_index_counter : integer := 0;	-- 0 to 3  for 1x4 4x4 matrices
	
	TYPE STATE_TYPE IS (s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s66);
	SIGNAL state : STATE_TYPE;	
	
BEGIN
	ui_mem_a : mem generic map(2, 8) port map(clk, a_write, a_address, input, A);
	ui_mem_r : mem generic map(2, (16 + integer(ceil(log2(real(4)))))) port map(clk, r_write, r_address, r_accumulator, result);
	ui_mem_h : mem_h port map(clk, h_address, B);
	ui_mul : mul port map(A, B, mul_result);
	
	h_address <= std_logic_vector(to_unsigned((a_index_counter * 4) + h_index_counter, h_address'length));
	r_address <= std_logic_vector(to_unsigned(r_index_counter, r_address'length));
	a_address <= std_logic_vector(to_unsigned(a_index_counter, a_address'length));
	
	PROCESS(clk, reset)
	BEGIN
		IF reset = '1' THEN
			state <= s0;
		ELSIF clk'EVENT and clk = '1' then			
			CASE state IS
				when s0 =>
					state <= s1;
				when s1 =>
					if enable = '1' then
						state <= s2;
					end if;
				when s2 =>
					if enable = '0' then
						state <= s3;
					end if;
					
					if a_index_counter = 3 then
						state <= s4;
					end if;
				when s3 =>
					state <= s1;
				when s4 =>
					state <= s5;
				when s5 =>
					state <= s6;
				when s6 =>
					if a_index_counter = 4 then
						state <= s7;
					else
						state <= s66;
					end if;
				when s66 =>
					state <= s5;
				when s7 =>
					state <= s8;
				when s8 =>
					if r_index_counter = 4 then
						state <= s9;
					else
						state <= s5;
					end if;
				when others =>
			end case;
		end if;
	end process;

	process(state)
	begin
		case state is
			when s0 =>
				r_write <= '0';
				a_write <= '0';
				r_accumulator <= "000000000000000000";
				a_index_counter <= 0;
				h_index_counter <= 0;
				r_index_counter <= 0;
			when s1 =>
			when s2 =>
				a_write <= '1';
			when s3 =>
				a_write <= '0';
				a_index_counter <= a_index_counter + 1;
			when s4 =>
				a_index_counter <= 0;
				a_write <= '0';
			when s5 =>
				r_accumulator <= r_accumulator + mul_result;
			when s6 =>
				a_index_counter <= a_index_counter + 1;
			when s66 =>
			when s7 =>
				r_write <= '1';
			when s8 =>
				r_write <= '0';
				r_accumulator <= "000000000000000000";
				r_index_counter <= r_index_counter + 1;
				h_index_counter <= h_index_counter + 1;
				a_index_counter <= 0;
			when others =>
				mul_complete <= '1';
				r_write <= '0';
		end case;
	end process;
	
END arc_mac_top;
